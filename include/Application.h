#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <string>
#include <vector>

class State;
class Assets;

class Application {
public:
    Application(const char* title,int width, int height);
    ~Application();
    void start();

    bool rectCollision(SDL_Rect rect1, SDL_Rect rect2);

    void changeState(State* state);
    void pushState(State* state);
    void popState();

private:
    void update();
    void render();

public:
    SDL_Window* window;
    SDL_Renderer* renderer;
    SDL_Event event;

    Assets* assets;

    bool running = true;
    bool justTyped = false;
    bool backSpacePressed = false;
    bool clicked = false;

    std::string typedText;
    int mouseX;
    int mouseY;

private:
    std::vector<State*> m_states;
};
