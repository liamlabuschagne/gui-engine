#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

class Application;

class GUIObject
{
public:
    GUIObject(Application* app,
    	SDL_Rect fgBounds, SDL_Rect bgBounds,
    	SDL_Texture* fgTexture, SDL_Texture* bgTexture,
    	TTF_Font* fontKey, SDL_Color color);
    ~GUIObject();
    virtual void update()=0;
    void render();
public:
	Application* m_app;

    SDL_Rect m_fgBounds;
    SDL_Rect m_bgBounds;

    SDL_Texture* m_fgTexture;
    SDL_Texture* m_bgTexture;

    TTF_Font* m_font;
    SDL_Color m_color;
};
