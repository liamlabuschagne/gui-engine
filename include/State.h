#pragma once
#include <vector>

class GUIObject;

class State {
public:
	State();
	void addGUIObject(GUIObject* obj);
	void update();
	void render();
private:
	std::vector<GUIObject*> m_objects;
};