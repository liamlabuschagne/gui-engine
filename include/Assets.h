#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <vector>
#include <algorithm>

class Application;

class Assets
{
public:
	Assets(Application* app);
	TTF_Font* createFont(const char* filePath, int ptSize);
	SDL_Texture* createTexture(SDL_Surface* surface);
	SDL_Texture* createTexture(TTF_Font* font, const char* text, SDL_Color color);
	SDL_Texture* createTexture(const char* filePath);
private:
	Application* m_app;
};
