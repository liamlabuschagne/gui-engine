#pragma once

#include "GUIObject.h"

class Button : public GUIObject {
public:
    using GUIObject::GUIObject;
    void init(const char* text);
    void changeText(const char* text);
    void update();
    void ulean();
    void (*onClick)(Button *btn);
};
 