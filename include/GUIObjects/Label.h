#pragma once

#include "GUIObject.h"

class Label : public GUIObject {
public:
	using GUIObject::GUIObject;
    void init(const char* text);
    void changeText(const char* text);
    void update();
};
