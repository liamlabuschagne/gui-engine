#pragma once

#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "GUIObject.h"

class Textbox : public GUIObject {
public:
	using GUIObject::GUIObject;
    void init(std::string text, SDL_Texture* focusedTexture, SDL_Texture* unFoucusedTexture);
    void changeText(const char* text);
    void update();
    std::string m_text;
private:
    std::string m_placeHolder;

    bool m_focused = false;

    SDL_Texture* m_focusedTexture;
    SDL_Texture* m_unFocusedTexture;
};
