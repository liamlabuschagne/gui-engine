#include "../include/Application.h"
#include "../include/Assets.h"
#include "../include/State.h"
Application::Application(const char* title,int width, int height)
{
    // initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        running = false;
        return;
    }

    // initialize TTF
    if(TTF_Init()==-1) {
        SDL_Log("TTF_init: %s\n", TTF_GetError());
        running = false;
        return;
    }

    // Setup Window
    window = SDL_CreateWindow(
        title,                  // window title
        SDL_WINDOWPOS_UNDEFINED,           // initial x position
        SDL_WINDOWPOS_UNDEFINED,           // initial y position
        width,                               // width, in pixels
        height,                               // height, in pixels
        SDL_WINDOW_OPENGL                  // flags - see below
    );

    // Check that the window was successfully created
    if (window == NULL) {
        // In the case that the window could not be made...
        SDL_Log("Could not create window: %s\n", SDL_GetError());
        running = false;
        return;
    }

    // Setup Renderer
    renderer = SDL_CreateRenderer(
        window,
        -1,
        SDL_RENDERER_ACCELERATED
    );

    // Check that the window was successfully created
    if (renderer == NULL) {
        // In the case that the window could not be made...
        SDL_Log("Could not create renderer: %s\n", SDL_GetError());
        running = false;
        return;
    }

    assets = new Assets(this);
}

Application::~Application()
{
    delete assets;
    if(m_states.size() > 0)
    {
        for(auto s : m_states)
            delete s;
    }
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    TTF_Quit();
    SDL_Quit();
}

void Application::start()
{
    unsigned int startTime = 0;
    unsigned int endTime = 0;
    while(running)
    {
        startTime = SDL_GetTicks();

        // Main loop
        update();
        render();

        endTime = SDL_GetTicks();
        if(startTime - endTime < 1000/30)
        {
          SDL_Delay( (1000/30) - (startTime-endTime) );
        }
    }
}

void Application::changeState(State* state)
{
    m_states.pop_back();
    m_states.push_back(state);
}

void Application::pushState(State* state)
{
    m_states.push_back(state);
}

void Application::popState()
{
    m_states.pop_back();
}

void Application::update()
{
    justTyped = false;
    backSpacePressed = false;
    clicked = false;
    while(SDL_PollEvent(&event))
    {
        switch(event.type)
        {
        case SDL_QUIT:
            running = false;
            break;
        case SDL_MOUSEBUTTONUP:
            clicked = true;
            break;
        case SDL_TEXTINPUT:
            typedText = event.text.text;
            justTyped = true;
            break;
        case SDL_KEYUP:
            switch(event.key.keysym.sym)
            {
            case SDLK_BACKSPACE:
                backSpacePressed = true;
                break;
            }
         break;
        }
    }
    SDL_GetMouseState(&mouseX,&mouseY);

    if(m_states.size() > 0)
        m_states.back()->update();
}

void Application::render()
{
    SDL_RenderClear(renderer);
    if(m_states.size() > 0)
        m_states.back()->render();
    
    SDL_RenderPresent(renderer);
}

bool Application::rectCollision(SDL_Rect rect1, SDL_Rect rect2)
{
   return (rect1.x < rect2.x + rect2.w &&
   rect1.x + rect1.w > rect2.x &&
   rect1.y < rect2.y + rect2.h &&
   rect1.h + rect1.y > rect2.y);
}
