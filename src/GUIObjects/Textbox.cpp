#include "../include/GUIObjects/Textbox.h"
#include "../include/Application.h"
#include "../include/Assets.h"

void Textbox::init(std::string text, SDL_Texture* focusedTexture, SDL_Texture* unFoucusedTexture)
{
    m_placeHolder = text;
    changeText(text.c_str());
    m_focusedTexture = focusedTexture;
    m_unFocusedTexture = unFoucusedTexture;
    TTF_SizeText(m_font, text.c_str(), &m_fgBounds.w, &m_fgBounds.h);
}

void Textbox::update()
{
    if(m_focused)
    {
        std::string tmp = m_text;
        if(m_app->justTyped)
            m_text.append(m_app->typedText);
        if(m_app->backSpacePressed && m_text.length() > 0)
        {
            m_text.pop_back();
            tmp = "";
        }
        if(m_text.length() > 0 && tmp != m_text)
            changeText(m_text.c_str());
        if(m_text.length() == 0)
            changeText(m_placeHolder.c_str());
    }

    if(m_app->clicked)
    {
        if(m_app->rectCollision({m_app->mouseX,m_app->mouseY,1,1},m_bgBounds))
        {
            m_focused = true;
            m_bgTexture = m_focusedTexture;
        }
        else
        {
            m_focused = false;
            m_bgTexture = m_unFocusedTexture;
        }
    }
}

void Textbox::changeText(const char* text)
{
    TTF_SizeText(m_font, text, &m_fgBounds.w, &m_fgBounds.h);
    if(m_fgBounds.w < m_bgBounds.w)
        m_fgTexture = m_app->assets->createTexture(m_font,text,m_color);
    else
    {
        m_text.pop_back();
        TTF_SizeText(m_font, text, &m_fgBounds.w, &m_fgBounds.h);
        m_fgTexture = m_app->assets->createTexture(m_font,text,m_color);
    }
}
