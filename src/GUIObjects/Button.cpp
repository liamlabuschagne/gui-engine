#include "../include/GUIObjects/Button.h"
#include "../include/Application.h"
#include "../include/Assets.h"

void Button::init(const char* text)
{
    changeText(text);
    m_bgBounds.x = m_fgBounds.x;
    m_bgBounds.y = m_fgBounds.y;
}

void Button::update()
{
    if(m_app->clicked && onClick && m_app->rectCollision(SDL_Rect({m_app->mouseX,m_app->mouseY,1,1}),m_bgBounds))
        onClick(this);
}

void Button::changeText(const char* text)
{
    m_fgTexture = m_app->assets->createTexture(m_font,text,m_color);
    TTF_SizeText(m_font, text, &m_fgBounds.w, &m_fgBounds.h);
}
