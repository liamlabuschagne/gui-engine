#include "../include/GUIObjects/Label.h"
#include "../include/Application.h"
#include "../include/Assets.h"

void Label::init(const char* text)
{
    changeText(text);
}

void Label::update()
{
}

void Label::changeText(const char* text)
{
    m_fgTexture = m_app->assets->createTexture(m_font,text,m_color);
    TTF_SizeText(m_font, text, &m_fgBounds.w, &m_fgBounds.h);
}
