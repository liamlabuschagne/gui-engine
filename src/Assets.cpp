#include "../include/Assets.h"
#include "../include/Application.h"
Assets::Assets(Application* app) : m_app(app)
{}

TTF_Font* Assets::createFont(const char* filePath, int ptSize)
{
    TTF_Font* font = TTF_OpenFont(filePath, ptSize);
    if(!font)
    {
        SDL_Log("TTF_OpenFont: %s",TTF_GetError());
        m_app->running = false;
        return nullptr;
    }
    return font;
}

SDL_Texture* Assets::createTexture(SDL_Surface* surface)
{
    SDL_Texture* texture = SDL_CreateTextureFromSurface(m_app->renderer,surface);
    if(!texture)
    {
        SDL_Log("SDL_CreateTextureFromSurface: %s", SDL_GetError());
        m_app->running = false;
        return nullptr;
    }
    return texture;
}

SDL_Texture* Assets::createTexture(TTF_Font* font, const char* text, SDL_Color color)
{
    SDL_Surface* surface = TTF_RenderText_Solid(font,text,color);
    if(!surface)
    {
        SDL_Log("TTF_RenderText_Solid: %s", TTF_GetError());
        m_app->running = false;
        return nullptr;
    }
    return createTexture(surface);
}

SDL_Texture* Assets::createTexture(const char* filePath)
{
    SDL_Surface* surface = IMG_Load(filePath);
    if(!surface)
    {
        SDL_Log("IMG_Load: %s", IMG_GetError());
        m_app->running = false;
        return nullptr;
    }
    return createTexture(surface);
}