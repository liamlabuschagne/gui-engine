#include "../include/GUIObject.h"
#include "../include/Application.h"
GUIObject::GUIObject(Application* app,
    	SDL_Rect fgBounds, SDL_Rect bgBounds,
    	SDL_Texture* fgTexture, SDL_Texture* bgTexture,
    	TTF_Font* font, SDL_Color color)
		: m_app(app), m_fgBounds(fgBounds), m_bgBounds(bgBounds),m_fgTexture(fgTexture), m_bgTexture(bgTexture), m_font(font), m_color(color)
{
}

GUIObject::~GUIObject()
{
	TTF_CloseFont(m_font);
	SDL_DestroyTexture(m_fgTexture);
	SDL_DestroyTexture(m_bgTexture);
}

void GUIObject::render()
{
	if(m_bgTexture)
    	SDL_RenderCopy(m_app->renderer,m_bgTexture,NULL,&m_bgBounds);
    if(m_fgTexture)
    	SDL_RenderCopy(m_app->renderer,m_fgTexture,NULL,&m_fgBounds);
}
