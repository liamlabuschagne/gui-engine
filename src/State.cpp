#include "../include/State.h"
#include "../include/GUIObject.h"
State::State()
{
}

void State::addGUIObject(GUIObject* obj)
{
	m_objects.push_back(obj);
}

void State::update()
{
	for(auto o : m_objects)
		o->update();
}

void State::render()
{
	for(auto o : m_objects)
		o->render();
}
