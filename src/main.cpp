#include <math.h>
#include "../include/Application.h"
#include "../include/GUIObject.h"
#include "../include/State.h"
#include "../include/Assets.h"
#include "../include/GUIObjects/Label.h"
#include "../include/GUIObjects/Button.h"
#include "../include/GUIObjects/Textbox.h"

Application* app;

State* menu;
Button* btnPythagoras;
Button* btnRectArea;
Button* btnCircleArea;
Button* btnHeightCalc;
Button* btnRadiusCalc;

Button* menuButton;

State* pythagoras;
Label* pythagorasTitle;
Textbox* pythagorasA;
Textbox* pythagorasB;
Label* pythagorasAnswer;
Button* pythagorasButton;

State* rectArea;
Label* rectAreaTitle;
Textbox* rectAreaW;
Textbox* rectAreaH;
Label* rectAreaAnswer;
Button* rectAreaButton;

State* circleArea;
Label* circleAreaTitle;
Textbox* circleAreaRadius;
Label* circleAreaAnswer;
Button* circleAreaButton;

State* heightCalc;
Label* heightCalcTitle;
Textbox* heightCalcSlopeDistance;
Textbox* heightCalcHorizontalDistance;
Textbox* heightCalcAngle;
Textbox* heightCalcMinutes;
Textbox* heightCalcSeconds;
Label* heightCalcAnswer;
Button* heightCalcButton;

State* radiusCalc;
Label* radiusCalcTitle;
Textbox* radiusCalcCircumfrence;
Label* radiusCalcAnswer;
Button* radiusCalcButton;

void btnMenuButtonClick(Button* btn)
{
    app->popState();
}

void btnPythagorasClick(Button* btn)
{
    app->pushState(pythagoras);
}

void btnRectAreaClick(Button* btn)
{
    app->pushState(rectArea);
}

void btnCircleAreaClick(Button* btn)
{
    app->pushState(circleArea);
}

void btnHeightCalcClick(Button* btn)
{
    app->pushState(heightCalc);
}

void btnRadiusCalcClick(Button* btn)
{
    app->pushState(radiusCalc);
}

void btnPythagorasCalcClick(Button* btn)
{
    double answer = sqrt(pow(std::atof(pythagorasA->m_text.c_str()),2) + pow(std::atof(pythagorasB->m_text.c_str()),2));
    std::string text = "=";
    text.append(std::to_string(answer));
    pythagorasAnswer->changeText(text.c_str());
}

void btnRectAreaCalcClick(Button* btn)
{
    double answer = std::atof(rectAreaW->m_text.c_str()) * std::atof(rectAreaH->m_text.c_str());
    std::string text = "=";
    text.append(std::to_string(answer));
    rectAreaAnswer->changeText(text.c_str());
}

const double PI = 3.14159265358979323846264338328;

void btnCircleAreaCalcClick(Button* btn)
{
    double answer = pow(std::atof(circleAreaRadius->m_text.c_str()),2) * PI;
    std::string text = "=";
    text.append(std::to_string(answer));
    circleAreaAnswer->changeText(text.c_str());
}

void btnHeightCalcCalcClick(Button* btn)
{
    double minutes = atof(heightCalcMinutes->m_text.c_str())/60;
    double seconds = atof(heightCalcSeconds->m_text.c_str())/3600;
    double angle = atof(heightCalcAngle->m_text.c_str());
    double distance = atof(heightCalcHorizontalDistance->m_text.c_str());
    double answer = distance * sinf( (90 - ( angle + (minutes + seconds) ) ) / 57.2958);
    std::string text = "=";
    text.append(std::to_string(answer));
    heightCalcAnswer->changeText(text.c_str());
}

void btnRadiusCalcCalcClick(Button* btn)
{
    double circumfrence = atof(radiusCalcCircumfrence->m_text.c_str());
    double answer = circumfrence / (2*PI);
    std::string text = "=";
    text.append(std::to_string(answer));
    radiusCalcAnswer->changeText(text.c_str());
}

int main(int argc, char** argv)
{
    app = new Application("Formulae",200,200);

    TTF_Font* arial = app->assets->createFont("../res/arial.ttf",16);
    SDL_Texture* button = app->assets->createTexture("../res/button.png");
    SDL_Texture* buttonPressed = app->assets->createTexture("../res/button_pressed.png");
    SDL_Color white = {255,255,255,255};
    
    SDL_Rect pos1 = {0,0,100,18};
    SDL_Rect pos2 = {0,20,100,18};
    SDL_Rect pos3 = {0,40,100,18};
    SDL_Rect pos4 = {0,60,100,18};
    SDL_Rect pos5 = {0,80,100,18};
    SDL_Rect pos6 = {0,100,100,18};
    SDL_Rect pos7 = {0,120,100,18};
    SDL_Rect pos8 = {0,140,100,18};

    // Menu State
    menu = new State();
    btnPythagoras = new Button(app,{0,0,130,18},{0,0,130,18},nullptr,button,arial,white);
    btnPythagoras->init("Pythagoras");
    btnPythagoras->onClick = btnPythagorasClick;
    menu->addGUIObject(btnPythagoras);

    btnRectArea = new Button(app,{0,20,130,18},{0,20,130,18},nullptr,button,arial,white);
    btnRectArea->init("Rectangle Area");
    btnRectArea->onClick = btnRectAreaClick;
    menu->addGUIObject(btnRectArea);

    btnRadiusCalc = new Button(app,{0,40,130,18},{0,40,130,18},nullptr,button,arial,white);
    btnRadiusCalc->init("Radius Calculation");
    btnRadiusCalc->onClick = btnRadiusCalcClick;
    menu->addGUIObject(btnRadiusCalc);

    btnCircleArea = new Button(app,{0,60,130,18},{0,60,130,18},nullptr,button,arial,white);
    btnCircleArea->init("Circle Area");
    btnCircleArea->onClick = btnCircleAreaClick;
    menu->addGUIObject(btnCircleArea);

    btnHeightCalc = new Button(app,{0,80,130,18},{0,80,130,18},nullptr,button,arial,white);
    btnHeightCalc->init("Height Calculation");
    btnHeightCalc->onClick = btnHeightCalcClick;
    menu->addGUIObject(btnHeightCalc);

    // Back to menu button for all states

    menuButton = new Button(app,pos8,pos8,nullptr,button,arial,white);
    menuButton->init("Menu");
    menuButton->onClick = btnMenuButtonClick;

    // Pythagoras state
    pythagoras = new State();

    pythagorasTitle = new Label(app,pos1,pos1,nullptr,nullptr,arial,white);
    pythagorasTitle->init("Pythagoras");
    pythagoras->addGUIObject(pythagorasTitle);

    pythagorasA = new Textbox(app,pos2,pos2,nullptr,button,arial,white);
    pythagorasA->init("A:",buttonPressed,button);
    pythagoras->addGUIObject(pythagorasA);

    pythagorasB = new Textbox(app,pos3,pos3,nullptr,button,arial,white);
    pythagorasB->init("B:",buttonPressed,button);
    pythagoras->addGUIObject(pythagorasB);

    pythagorasAnswer = new Label(app,pos4,pos4,nullptr,nullptr,arial,white);
    pythagorasAnswer->init("=");
    pythagoras->addGUIObject(pythagorasAnswer);

    pythagorasButton = new Button(app,pos5,pos5,nullptr,button,arial,white);
    pythagorasButton->init("Calculate");
    pythagorasButton->onClick = btnPythagorasCalcClick;
    pythagoras->addGUIObject(pythagorasButton);

    pythagoras->addGUIObject(menuButton);

    // Rectangle Area state
    rectArea = new State();

    rectAreaTitle = new Label(app,{0,0,110,18},{0,0,110,18},nullptr,nullptr,arial,white);
    rectAreaTitle->init("Rectangle Area");
    rectArea->addGUIObject(rectAreaTitle);

    rectAreaW = new Textbox(app,pos2,pos2,nullptr,button,arial,white);
    rectAreaW->init("Width:",buttonPressed,button);
    rectArea->addGUIObject(rectAreaW);

    rectAreaH = new Textbox(app,pos3,pos3,nullptr,button,arial,white);
    rectAreaH->init("Height:",buttonPressed,button);
    rectArea->addGUIObject(rectAreaH);

    rectAreaAnswer = new Label(app,pos4,pos4,nullptr,nullptr,arial,white);
    rectAreaAnswer->init("=");
    rectArea->addGUIObject(rectAreaAnswer);

    rectAreaButton = new Button(app,pos5,pos5,nullptr,button,arial,white);
    rectAreaButton->init("Calculate");
    rectAreaButton->onClick = btnRectAreaCalcClick;
    rectArea->addGUIObject(rectAreaButton);

    rectArea->addGUIObject(menuButton);

    // Radius Calculation state
    radiusCalc = new State();

    heightCalcTitle = new Label(app,pos1,pos1,nullptr,nullptr,arial,white);
    heightCalcTitle->init("Radius Calculation");
    radiusCalc->addGUIObject(heightCalcTitle);

    radiusCalcCircumfrence = new Textbox(app,pos2,pos2,nullptr,button,arial,white);
    radiusCalcCircumfrence->init("Circumfrence:",buttonPressed,button);
    radiusCalc->addGUIObject(radiusCalcCircumfrence);

    radiusCalcAnswer = new Label(app,pos3,pos3,nullptr,nullptr,arial,white);
    radiusCalcAnswer->init("=");
    radiusCalc->addGUIObject(radiusCalcAnswer);

    radiusCalcButton = new Button(app,pos4,pos4,nullptr,button,arial,white);
    radiusCalcButton->init("Calculate");
    radiusCalcButton->onClick = btnRadiusCalcCalcClick;
    radiusCalc->addGUIObject(radiusCalcButton);

    radiusCalc->addGUIObject(menuButton);

    // Circle Area state
    circleArea = new State();

    circleAreaTitle = new Label(app,pos1,pos1,nullptr,nullptr,arial,white);
    circleAreaTitle->init("Circle Area");
    circleArea->addGUIObject(circleAreaTitle);

    circleAreaRadius = new Textbox(app,pos2,pos2,nullptr,button,arial,white);
    circleAreaRadius->init("Radius:",buttonPressed,button);
    circleArea->addGUIObject(circleAreaRadius);

    circleAreaAnswer = new Label(app,pos3,pos3,nullptr,nullptr,arial,white);
    circleAreaAnswer->init("=");
    circleArea->addGUIObject(circleAreaAnswer);

    circleAreaButton = new Button(app,pos4,pos4,nullptr,button,arial,white);
    circleAreaButton->init("Calculate");
    circleAreaButton->onClick = btnCircleAreaCalcClick;
    circleArea->addGUIObject(circleAreaButton);

    circleArea->addGUIObject(menuButton);

    // Height Calculation state
    heightCalc = new State();

    heightCalcTitle = new Label(app,pos1,pos1,nullptr,nullptr,arial,white);
    heightCalcTitle->init("Height Calculation");
    heightCalc->addGUIObject(heightCalcTitle);

    heightCalcHorizontalDistance = new Textbox(app,pos2,pos2,nullptr,button,arial,white);
    heightCalcHorizontalDistance->init("Distance:",buttonPressed,button);
    heightCalc->addGUIObject(heightCalcHorizontalDistance);

    heightCalcAngle = new Textbox(app,pos3,pos3,nullptr,button,arial,white);
    heightCalcAngle->init("Angle:",buttonPressed,button);
    heightCalc->addGUIObject(heightCalcAngle);

    heightCalcMinutes = new Textbox(app,pos4,pos4,nullptr,button,arial,white);
    heightCalcMinutes->init("Minutes:",buttonPressed,button);
    heightCalc->addGUIObject(heightCalcMinutes);

    heightCalcSeconds = new Textbox(app,pos5,pos5,nullptr,button,arial,white);
    heightCalcSeconds->init("Seconds:",buttonPressed,button);
    heightCalc->addGUIObject(heightCalcSeconds);

    heightCalcAnswer = new Label(app,pos6,pos6,nullptr,nullptr,arial,white);
    heightCalcAnswer->init("=");
    heightCalc->addGUIObject(heightCalcAnswer);

    heightCalcButton = new Button(app,pos7,pos7,nullptr,button,arial,white);
    heightCalcButton->init("Calculate");
    heightCalcButton->onClick = btnHeightCalcCalcClick;
    heightCalc->addGUIObject(heightCalcButton);

    heightCalc->addGUIObject(menuButton);

    app->pushState(menu);
    app->start();
    delete app;
    return 0;
}
